# Ensi\FeedClient\CommonApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**migrateEntities**](CommonApi.md#migrateEntities) | **POST** /common/entities:migrate | Start synchronization of data objects from master services



## migrateEntities

> \Ensi\FeedClient\Dto\EmptyDataResponse migrateEntities()

Start synchronization of data objects from master services

Start synchronization of data objects from master services

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->migrateEntities();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->migrateEntities: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\FeedClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

