# Ensi\FeedClient\FeedSettingsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFeedSettings**](FeedSettingsApi.md#createFeedSettings) | **POST** /feeds/feed-settings | Создание объекта типа FeedSettings
[**getFeedSettings**](FeedSettingsApi.md#getFeedSettings) | **GET** /feeds/feed-settings/{id} | Получение объекта типа FeedSettings
[**patchFeedSettings**](FeedSettingsApi.md#patchFeedSettings) | **PATCH** /feeds/feed-settings/{id} | Обновления части полей объекта типа FeedSettings
[**searchFeedSettings**](FeedSettingsApi.md#searchFeedSettings) | **POST** /feeds/feed-settings:search | Поиск объектов типа FeedSettings



## createFeedSettings

> \Ensi\FeedClient\Dto\FeedSettingsResponse createFeedSettings($feed_settings_for_create)

Создание объекта типа FeedSettings

Создание объекта типа FeedSettings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\FeedSettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$feed_settings_for_create = new \Ensi\FeedClient\Dto\FeedSettingsForCreate(); // \Ensi\FeedClient\Dto\FeedSettingsForCreate | 

try {
    $result = $apiInstance->createFeedSettings($feed_settings_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FeedSettingsApi->createFeedSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feed_settings_for_create** | [**\Ensi\FeedClient\Dto\FeedSettingsForCreate**](../Model/FeedSettingsForCreate.md)|  |

### Return type

[**\Ensi\FeedClient\Dto\FeedSettingsResponse**](../Model/FeedSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getFeedSettings

> \Ensi\FeedClient\Dto\FeedSettingsResponse getFeedSettings($id)

Получение объекта типа FeedSettings

Получение объекта типа FeedSettings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\FeedSettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getFeedSettings($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FeedSettingsApi->getFeedSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\FeedClient\Dto\FeedSettingsResponse**](../Model/FeedSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchFeedSettings

> \Ensi\FeedClient\Dto\FeedSettingsResponse patchFeedSettings($id, $feed_settings_for_patch)

Обновления части полей объекта типа FeedSettings

Обновления части полей объекта типа FeedSettings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\FeedSettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$feed_settings_for_patch = new \Ensi\FeedClient\Dto\FeedSettingsForPatch(); // \Ensi\FeedClient\Dto\FeedSettingsForPatch | 

try {
    $result = $apiInstance->patchFeedSettings($id, $feed_settings_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FeedSettingsApi->patchFeedSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **feed_settings_for_patch** | [**\Ensi\FeedClient\Dto\FeedSettingsForPatch**](../Model/FeedSettingsForPatch.md)|  |

### Return type

[**\Ensi\FeedClient\Dto\FeedSettingsResponse**](../Model/FeedSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchFeedSettings

> \Ensi\FeedClient\Dto\SearchFeedSettingsResponse searchFeedSettings($search_feed_settings_request)

Поиск объектов типа FeedSettings

Поиск объектов типа FeedSettings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\FeedSettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_feed_settings_request = new \Ensi\FeedClient\Dto\SearchFeedSettingsRequest(); // \Ensi\FeedClient\Dto\SearchFeedSettingsRequest | 

try {
    $result = $apiInstance->searchFeedSettings($search_feed_settings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FeedSettingsApi->searchFeedSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_feed_settings_request** | [**\Ensi\FeedClient\Dto\SearchFeedSettingsRequest**](../Model/SearchFeedSettingsRequest.md)|  |

### Return type

[**\Ensi\FeedClient\Dto\SearchFeedSettingsResponse**](../Model/SearchFeedSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

