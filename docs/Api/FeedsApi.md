# Ensi\FeedClient\FeedsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getFeed**](FeedsApi.md#getFeed) | **GET** /feeds/feeds/{id} | Получение объекта типа Feed
[**searchFeeds**](FeedsApi.md#searchFeeds) | **POST** /feeds/feeds:search | Поиск объектов типа Feed



## getFeed

> \Ensi\FeedClient\Dto\FeedResponse getFeed($id)

Получение объекта типа Feed

Получение объекта типа Feed

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\FeedsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getFeed($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FeedsApi->getFeed: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\FeedClient\Dto\FeedResponse**](../Model/FeedResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchFeeds

> \Ensi\FeedClient\Dto\SearchFeedsResponse searchFeeds($search_feeds_request)

Поиск объектов типа Feed

Поиск объектов типа Feed

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\FeedsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_feeds_request = new \Ensi\FeedClient\Dto\SearchFeedsRequest(); // \Ensi\FeedClient\Dto\SearchFeedsRequest | 

try {
    $result = $apiInstance->searchFeeds($search_feeds_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FeedsApi->searchFeeds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_feeds_request** | [**\Ensi\FeedClient\Dto\SearchFeedsRequest**](../Model/SearchFeedsRequest.md)|  |

### Return type

[**\Ensi\FeedClient\Dto\SearchFeedsResponse**](../Model/SearchFeedsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

