# Ensi\FeedClient\CloudIntegrationsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCloudIntegration**](CloudIntegrationsApi.md#createCloudIntegration) | **POST** /cloud/cloud-integrations | Создание объекта типа CloudIntegration
[**getCloudIntegration**](CloudIntegrationsApi.md#getCloudIntegration) | **GET** /cloud/cloud-integrations | Получение объекта типа CloudIntegration
[**patchCloudIntegration**](CloudIntegrationsApi.md#patchCloudIntegration) | **PATCH** /cloud/cloud-integrations | Изменение объектов типа CloudIntegration



## createCloudIntegration

> \Ensi\FeedClient\Dto\CloudIntegrationResponse createCloudIntegration($create_cloud_integration_request)

Создание объекта типа CloudIntegration

Создание объекта типа CloudIntegration

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\CloudIntegrationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_cloud_integration_request = new \Ensi\FeedClient\Dto\CreateCloudIntegrationRequest(); // \Ensi\FeedClient\Dto\CreateCloudIntegrationRequest | 

try {
    $result = $apiInstance->createCloudIntegration($create_cloud_integration_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CloudIntegrationsApi->createCloudIntegration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_cloud_integration_request** | [**\Ensi\FeedClient\Dto\CreateCloudIntegrationRequest**](../Model/CreateCloudIntegrationRequest.md)|  |

### Return type

[**\Ensi\FeedClient\Dto\CloudIntegrationResponse**](../Model/CloudIntegrationResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCloudIntegration

> \Ensi\FeedClient\Dto\CloudIntegrationResponse getCloudIntegration()

Получение объекта типа CloudIntegration

Получение объекта типа CloudIntegration

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\CloudIntegrationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getCloudIntegration();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CloudIntegrationsApi->getCloudIntegration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\FeedClient\Dto\CloudIntegrationResponse**](../Model/CloudIntegrationResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchCloudIntegration

> \Ensi\FeedClient\Dto\CloudIntegrationResponse patchCloudIntegration($patch_cloud_integration_request)

Изменение объектов типа CloudIntegration

Изменение объектов типа CloudIntegration

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\FeedClient\Api\CloudIntegrationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$patch_cloud_integration_request = new \Ensi\FeedClient\Dto\PatchCloudIntegrationRequest(); // \Ensi\FeedClient\Dto\PatchCloudIntegrationRequest | 

try {
    $result = $apiInstance->patchCloudIntegration($patch_cloud_integration_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CloudIntegrationsApi->patchCloudIntegration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_cloud_integration_request** | [**\Ensi\FeedClient\Dto\PatchCloudIntegrationRequest**](../Model/PatchCloudIntegrationRequest.md)|  |

### Return type

[**\Ensi\FeedClient\Dto\CloudIntegrationResponse**](../Model/CloudIntegrationResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

