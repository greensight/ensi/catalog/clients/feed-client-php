# # SearchFeedSettingsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\FeedClient\Dto\FeedSettings[]**](FeedSettings.md) |  | 
**meta** | [**\Ensi\FeedClient\Dto\SearchFeedsResponseMeta**](SearchFeedsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


