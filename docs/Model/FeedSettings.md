# # FeedSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**name** | **string** | Название фида | [optional] 
**code** | **string** | Код | [optional] 
**active** | **bool** | Активность фида | [optional] 
**type** | **int** | Тип фида из FeedTypeEnum | [optional] 
**platform** | **int** | Платформа фида из FeedPlatformEnum | [optional] 
**active_product** | **bool** | Добавлять в фид только активные товары | [optional] 
**active_category** | **bool** | Добавлять в фид только активные категории | [optional] 
**shop_name** | **string** | Название магазина | [optional] 
**shop_url** | **string** | Сайт магазина | [optional] 
**shop_company** | **string** | Название организации | [optional] 
**update_time** | **int** | Частота обновления (в часах) | [optional] 
**delete_time** | **int** | Время хранения старых версий фидов (в днях) | [optional] 
**feeds** | [**\Ensi\FeedClient\Dto\Feed[]**](Feed.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


