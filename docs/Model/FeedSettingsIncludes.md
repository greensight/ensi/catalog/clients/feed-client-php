# # FeedSettingsIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feeds** | [**\Ensi\FeedClient\Dto\Feed[]**](Feed.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


