# # FeedIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feed_settings** | [**\Ensi\FeedClient\Dto\FeedSettings**](FeedSettings.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


