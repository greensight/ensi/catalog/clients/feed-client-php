# # Feed

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**code** | **string** | Код фида | 
**file** | [**\Ensi\FeedClient\Dto\File**](File.md) |  | 
**planned_delete_at** | [**\DateTime**](\DateTime.md) | плановаяя дата удаления фида | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**feed_settings** | [**\Ensi\FeedClient\Dto\FeedSettings**](FeedSettings.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


