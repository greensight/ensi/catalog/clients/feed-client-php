# # CloudIntegration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | [**\DateTime**](\DateTime.md) | Время создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления | 
**private_api_key** | **string** | Приватный API-ключ | [optional] 
**public_api_key** | **string** | Публичный API-ключ | [optional] 
**integration** | **bool** | Интеграция с сервисом Ensi Cloud | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


