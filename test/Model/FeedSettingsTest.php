<?php
/**
 * FeedSettingsTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\FeedClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Feed
 *
 * Ensi Feed
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\FeedClient;

use PHPUnit\Framework\TestCase;

/**
 * FeedSettingsTest Class Doc Comment
 *
 * @category    Class
 * @description FeedSettings
 * @package     Ensi\FeedClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class FeedSettingsTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "FeedSettings"
     */
    public function testFeedSettings()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "code"
     */
    public function testPropertyCode()
    {
    }

    /**
     * Test attribute "active"
     */
    public function testPropertyActive()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "platform"
     */
    public function testPropertyPlatform()
    {
    }

    /**
     * Test attribute "active_product"
     */
    public function testPropertyActiveProduct()
    {
    }

    /**
     * Test attribute "active_category"
     */
    public function testPropertyActiveCategory()
    {
    }

    /**
     * Test attribute "shop_name"
     */
    public function testPropertyShopName()
    {
    }

    /**
     * Test attribute "shop_url"
     */
    public function testPropertyShopUrl()
    {
    }

    /**
     * Test attribute "shop_company"
     */
    public function testPropertyShopCompany()
    {
    }

    /**
     * Test attribute "update_time"
     */
    public function testPropertyUpdateTime()
    {
    }

    /**
     * Test attribute "delete_time"
     */
    public function testPropertyDeleteTime()
    {
    }

    /**
     * Test attribute "feeds"
     */
    public function testPropertyFeeds()
    {
    }
}
