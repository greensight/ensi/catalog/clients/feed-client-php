<?php

namespace Ensi\FeedClient;

class FeedClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\FeedClient\Api\FeedsApi',
        '\Ensi\FeedClient\Api\CloudIntegrationsApi',
        '\Ensi\FeedClient\Api\CommonApi',
        '\Ensi\FeedClient\Api\FeedSettingsApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\FeedClient\Dto\Feed',
        '\Ensi\FeedClient\Dto\FeedSettingsForPatch',
        '\Ensi\FeedClient\Dto\PatchCloudIntegrationRequest',
        '\Ensi\FeedClient\Dto\SearchFeedsResponse',
        '\Ensi\FeedClient\Dto\CloudIntegrationFillableProperties',
        '\Ensi\FeedClient\Dto\ErrorResponse',
        '\Ensi\FeedClient\Dto\SearchFeedsResponseMeta',
        '\Ensi\FeedClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\FeedClient\Dto\ErrorResponse2',
        '\Ensi\FeedClient\Dto\FeedSettingsReadonlyProperties',
        '\Ensi\FeedClient\Dto\RequestBodyCursorPagination',
        '\Ensi\FeedClient\Dto\FeedSettingsResponse',
        '\Ensi\FeedClient\Dto\EmptyDataResponse',
        '\Ensi\FeedClient\Dto\FeedSettingsFillableProperties',
        '\Ensi\FeedClient\Dto\Error',
        '\Ensi\FeedClient\Dto\PaginationTypeEnum',
        '\Ensi\FeedClient\Dto\CreateCloudIntegrationRequest',
        '\Ensi\FeedClient\Dto\FeedSettingsForCreate',
        '\Ensi\FeedClient\Dto\FeedReadonlyProperties',
        '\Ensi\FeedClient\Dto\File',
        '\Ensi\FeedClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\FeedClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\FeedClient\Dto\ResponseBodyPagination',
        '\Ensi\FeedClient\Dto\FeedSettings',
        '\Ensi\FeedClient\Dto\SearchFeedsRequest',
        '\Ensi\FeedClient\Dto\FeedTypeEnum',
        '\Ensi\FeedClient\Dto\CloudIntegrationResponse',
        '\Ensi\FeedClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\FeedClient\Dto\FeedPlatformEnum',
        '\Ensi\FeedClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\FeedClient\Dto\CloudIntegrationReadonlyProperties',
        '\Ensi\FeedClient\Dto\FeedSettingsIncludes',
        '\Ensi\FeedClient\Dto\SearchFeedSettingsRequest',
        '\Ensi\FeedClient\Dto\SearchFeedSettingsResponse',
        '\Ensi\FeedClient\Dto\FeedResponse',
        '\Ensi\FeedClient\Dto\FeedIncludes',
        '\Ensi\FeedClient\Dto\RequestBodyPagination',
        '\Ensi\FeedClient\Dto\ModelInterface',
        '\Ensi\FeedClient\Dto\CloudIntegration',
    ];

    /** @var string */
    public static $configuration = '\Ensi\FeedClient\Configuration';
}
